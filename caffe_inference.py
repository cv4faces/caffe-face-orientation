import numpy as np
import caffe

protoFile = "deploy.prototxt"
weightsFile = "face_orientation_iter_100.caffemodel"
imageFile = "test_images/left.jpg"
class2label = []
with open("class2label.txt") as fi:
    data = fi.readlines()

for i in range(len(data)):
    cl, label = data[i].strip().split()
    class2label.append(label)

input_height, input_width = 224, 224
img_mean = np.array([104, 117, 123], dtype=np.float32)

caffe.set_mode_gpu()
net = caffe.Net(protoFile, weightsFile, caffe.TEST)

im = caffe.io.load_image(imageFile)
im = caffe.io.resize_image(im, [input_height, input_width])

transformer = caffe.io.Transformer({'data': net.blobs['data'].data.shape})
transformer.set_transpose('data', (2, 0, 1))  # row to col
transformer.set_channel_swap('data', (2, 1, 0))  # RGB to BGR
transformer.set_raw_scale('data', 255)  # [0,1] to [0,255]
transformer.set_mean('data', img_mean)

net.blobs['data'].reshape(1, 3, input_height, input_width)
net.blobs['data'].data[...] = transformer.preprocess('data', im)
out = net.forward()
prob = out['prob']
pred_class = np.argmax(prob)

print('Predicted Class : {} , Confidence : {}'.format(class2label[pred_class], prob[pred_class]))