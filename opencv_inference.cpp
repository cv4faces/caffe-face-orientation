#include <opencv2/dnn.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/core/utils/trace.hpp>
using namespace cv;
using namespace cv::dnn;

#include <fstream>
#include <iostream>
#include <cstdlib>
using namespace std;

static void getMaxClass(const Mat &probBlob, int *classId, double *classProb)
{
    Mat probMat = probBlob.reshape(1, 1); 
    Point classNumber;

    minMaxLoc(probMat, NULL, classProb, NULL, &classNumber);
    *classId = classNumber.x;
}


int main(int argc, char **argv)
{
    String protoFile = "deploy.prototxt";
    String weightsFile = "face_orientation_iter_100.caffemodel";
    String imageFile = "test_images/left.jpg";

    Net net;
    net = dnn::readNetFromCaffe(protoFile, weightsFile);

    Mat img = imread(imageFile);
    Mat inputBlob = blobFromImage(img, 1.0f, Size(224, 224),
                                  Scalar(104, 117, 123), false, false);   
    net.setInput(inputBlob, "data");        
    Mat prob = net.forward("prob");         

    int classId;
    double classProb;
    getMaxClass(prob, &classId, &classProb);
    
    std::cout << "Predicted class: #" << classId << endl;
    std::cout << "Confidence : " << classProb << endl;
    cv::imshow("image", img);
    cv::waitKey(0);

    return 0;
}
