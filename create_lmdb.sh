set -e

EXAMPLE=.
# NOT Required for Colab
#TOOLS=/home/vikas/softwares/caffe/build/tools

TRAIN_DATA_ROOT=./
VAL_DATA_ROOT=./

RESIZE_HEIGHT=256
RESIZE_WIDTH=256

echo "Creating train lmdb..."

GLOG_logtostderr=1 $TOOLS/convert_imageset \
    --resize_height=$RESIZE_HEIGHT \
    --resize_width=$RESIZE_WIDTH \
    --shuffle \
    $TRAIN_DATA_ROOT \
    train_files.txt \
    $EXAMPLE/train_lmdb

echo "Creating val lmdb..."

GLOG_logtostderr=1 $TOOLS/convert_imageset \
    --resize_height=$RESIZE_HEIGHT \
    --resize_width=$RESIZE_WIDTH \
    --shuffle \
    $VAL_DATA_ROOT \
    val_files.txt \
    $EXAMPLE/val_lmdb

echo "Done."
