from __future__ import print_function
import numpy as np
import caffe
import cv2

protoFile = "deploy.prototxt"
weightsFile = "face_orientation_iter_100.caffemodel"
imageFile = "test_images/left.jpg"

input_height, input_width = 224, 224
img_mean = np.array([104, 117, 123])

net = cv2.dnn.readNetFromCaffe(protoFile, weightsFile)

im = cv2.imread(imageFile)
im = cv2.resize(im, (input_height, input_width))

blob = cv2.dnn.blobFromImage(im, 1, (input_height, input_width), img_mean, 0, 0)
net.setInput(blob)

out = net.forward('prob')
prob = out[0]
pred_class = np.argmax(prob)
cv2.imshow("Image", im)
print('Class : #{} , Confidence : {}'.format(pred_class, prob[pred_class]))

cv2.waitKey(0)
