import os, glob
 

TRAIN_DATA_DIR = "data/train"
VAL_DATA_DIR = "data/val"

trainFolders = glob.glob("{}/*".format(TRAIN_DATA_DIR))
valFolders = glob.glob("{}/*".format(VAL_DATA_DIR))

trainFolders.sort()
valFolders.sort()

train_file = open("train_files.txt","w")
val_file = open("val_files.txt","w")

for i in range(len(trainFolders)):
    files = glob.glob("{}/*".format(trainFolders[i]))
    for fi in files:
        train_file.writelines('{} {}\n'.format(fi, i))
train_file.close()

for i in range(len(valFolders)):
    files = glob.glob("{}/*".format(valFolders[i]))
    for fi in files:
        val_file.writelines('{} {}\n'.format(fi, i))
val_file.close()

class2label = ["Straight", "Right", "Left", "Upside-Down"]
class_mapping_file = open("class2label.txt","w")
for cl, label in enumerate(class2label):
    class_mapping_file.writelines("{} {}\n".format(cl, label))
class_mapping_file.close()