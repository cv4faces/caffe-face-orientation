import glob
import cv2
import os


# Create subfolders using the helen_5 folder
# Divide the total number of images into 4 parts 
# and move them into coressponding foler
valFiles = glob.glob("data/val/helen_5/*")

for i in range(4):
    os.makedirs("data/val/helen_{}".format(i+1))
    for j in range( int(i*len(valFiles)/4) , int((i+1)*len(valFiles)/4) ):
        os.rename(valFiles[j], "data/val/helen_{}/{}.jpg".format(i+1, j))


folders = ["helen_2", "helen_3", "helen_4"]
rotation = [cv2.ROTATE_90_CLOCKWISE, cv2.ROTATE_90_COUNTERCLOCKWISE, cv2.ROTATE_180]

for index, folder in enumerate(folders):
    print("processing {}".format(folder))
    trainFiles = glob.glob("data/train/{}/*".format(folder))
    valFiles = glob.glob("data/val/{}/*".format(folder))
    for fi in trainFiles:
        im = cv2.imread(fi)
        im = cv2.rotate(im, rotation[index])
        cv2.imwrite(fi,im)
    for fi in valFiles:
        im = cv2.imread(fi)
        im = cv2.rotate(im, rotation[index])
        cv2.imwrite(fi,im)

print("New Dataset created")

